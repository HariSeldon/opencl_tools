set(EXE_NAME matrixMul)

set(MM_SRC_DIR "src")
set(MM_INCLUDE_DIR "include")
set(MM_KERNEL_DIR "kernels")

set(MM_SRC_PATH "${MATRIX_MULTIPLICATION_PATH}/${MM_SRC_DIR}")
set(MM_INCLUDE_PATH "${MATRIX_MULTIPLICATION_PATH}/${MM_INCLUDE_DIR}")
set(MM_KERNEL_PATH \"${MATRIX_MULTIPLICATION_PATH}/${MM_KERNEL_DIR}\")

set(OPENCL_WRAPPER_INCLUDE_PATH ${TOOLS_PATH}/${OPENCL_WRAPPER_DIR}/${OPENCL_WRAPPER_INCLUDE_DIR}/${OPENCL_WRAPPER_CODE_LIB_DIR})

# Src files.
file(GLOB MM_SRC_FILES_LIST "${MM_SRC_PATH}/*.cpp")

add_executable(${EXE_NAME} ${MM_SRC_FILES_LIST})

configure_file("${MM_INCLUDE_PATH}/SystemConfiguration.h.cmake"
               "${MM_INCLUDE_PATH}/SystemConfiguration.h")

include_directories(${MM_INCLUDE_PATH})
include_directories(${OPENCL_WRAPPER_INCLUDE_PATH})
include_directories(${OPENCL_INCLUDE_PATHS})

#-------------------------------------------------------------------------------
# Libraries.

# OpenCL library.                                                             
target_link_libraries(${EXE_NAME} ${OPENCL_LIB_PATH})                         
                                                                              
# OpenCLWrapper Utils library.
link_directories(${CMAKE_BINARY_DIR}/${TOOLS_DIR}/${OPENCL_WRAPPER_DIR}/${OPENCL_WRAPPER_LIB_DIR}/${OPENCL_WRAPPER_UTILS_LIB_DIR})
target_link_libraries(${EXE_NAME} ${OPENCL_WRAPPER_UTILS_LIB})
                                                                              
# OpenCLWrapper library.              
link_directories(${CMAKE_BINARY_DIR}/${TOOLS_DIR}/${OPENCL_WRAPPER_DIR}/${OPENCL_WRAPPER_LIB_DIR}/${OPENCL_WRAPPER_CODE_LIB_DIR})
target_link_libraries(${EXE_NAME} ${OPENCL_WRAPPER_LIB})

