#include <iostream>
#include <cstdlib>
#include <sstream>
#include <string>

#include "Buffer.h"
#include "Device.h"
#include "Event.h"
#include "Kernel.h"
#include "Platform.h"
#include "Program.h"
#include "Queue.h"
#include "SystemConfiguration.h"

//-----------------------------------------------------------------------------
#define REPETITIONS 10
#define ELEMENT_LIMIT 5
#define DIMENSIONS 2
#define BLOCK_SIZE 16
#define SIZE BLOCK_SIZE * 10
#define DEVICE_ID 0
#define PLATFORM_ID 0
#define KERNEL_FILE_NAME "mm.cl"

//-----------------------------------------------------------------------------
void initialization(int argc, char** argv);
void hostMemoryAlloc();
void deviceMemoryAlloc();
void setKernelArguments();
void setKernelArgumentsWithLocal();
void setKernelArgumentsClassic();
void readResult();
void enqueWriteCommands(Queue& queue);
void enqueReadCommands(Queue& queue);
float random(float rand_min, float rand_max);
void run(const Context* context, Queue& queue);
void freeMemory();
void printVector(const float* vector, unsigned int size); 
void printMatrix(const float* matrix);
void setNDRangeSizes();
void verifyResults();

//-----------------------------------------------------------------------------
// Runtime components.
Platform* platform;
Kernel* kernel;

// Host data.
float* hostA;
float* hostB;
float* hostC;

// Device data.
Buffer* A;
Buffer* B;
Buffer* C;

cl_uint* height = NULL;
cl_uint* width = NULL;

size_t* localWorkSize = NULL;
size_t* globalWorkSize = NULL;

std::string kernelName = "";

//-----------------------------------------------------------------------------
int main(int argc, char** argv) {
  initialization(argc, argv);
  platform = new Platform(PLATFORM_ID);
  Context* context = platform->getContext();
  Device device = platform->getDevice(DEVICE_ID);
  std::cout << "Running " << kernelName << " on " << device.getName() << "\n";
  hostMemoryAlloc();
  deviceMemoryAlloc();
  std::string kernelFile = MM_KERNEL_PATH "/" KERNEL_FILE_NAME;
  Program program(context, kernelFile);
  Queue queue(*context, device, Queue::EnableProfiling);
  enqueWriteCommands(queue);
  if(!program.build(device)) {
    std::cout << "Error building the program: " << "\n";
    std::cout << program.getBuildLog(device) << "\n";
    return 1;
  }
  kernel = program.createKernel(kernelName.c_str());

  setKernelArguments();
  setNDRangeSizes();
  run(context, queue);
  enqueReadCommands(queue);
  verifyResults();
  freeMemory();
  return 0;
}

//-----------------------------------------------------------------------------
void initialization(int argc, char** argv) {
  srand((unsigned)time(0));

  if(argc == 2)
    kernelName = argv[1];
  else {
    std::cout << "Error passing the parameters: kernelName required." << 
                 std::endl; 
    exit(1);
  }
}

//-----------------------------------------------------------------------------
void setNDRangeSizes() {
  localWorkSize = new size_t[2];
  globalWorkSize = new size_t[2];

  localWorkSize[0] = BLOCK_SIZE;
  localWorkSize[1] = BLOCK_SIZE;
  globalWorkSize[0] = SIZE;
  globalWorkSize[1] = SIZE;
}

//-----------------------------------------------------------------------------
void freeMemory() {
  delete [] hostA;
  delete [] hostB;
  delete [] hostC;
  delete A;
  delete B;
  delete C;
  delete kernel;
  delete platform;
  delete width;
  delete height;
  delete [] localWorkSize;
  delete [] globalWorkSize;
}

//-----------------------------------------------------------------------------
void hostMemoryAlloc() {
  hostA = new float [SIZE * SIZE];
  hostB = new float [SIZE * SIZE];
  hostC = new float [SIZE * SIZE];

  for(unsigned int row = 0; row < SIZE; row++) {
    for(unsigned int column = 0; column < SIZE; column++) {
      hostA[column + SIZE * row] = random(-ELEMENT_LIMIT, ELEMENT_LIMIT);
      hostB[column + SIZE * row] = random(-ELEMENT_LIMIT, ELEMENT_LIMIT);
    }
  }
}

//-----------------------------------------------------------------------------
void deviceMemoryAlloc() {
  C = new Buffer(*(platform->getContext()), Buffer::WriteOnly,
                 SIZE * SIZE * sizeof(float), NULL);
  A = new Buffer(*(platform->getContext()), Buffer::ReadOnly,
                 SIZE * SIZE * sizeof(float), NULL);
  B = new Buffer(*(platform->getContext()), Buffer::ReadOnly,
                 SIZE * SIZE * sizeof(float), NULL);
  width = new cl_uint(SIZE);
  height = new cl_uint(SIZE);
}

//-----------------------------------------------------------------------------
void enqueWriteCommands(Queue& queue) {
  queue.writeBuffer(*A, SIZE * SIZE * sizeof(float), (void*) hostA);
  queue.writeBuffer(*B, SIZE * SIZE * sizeof(float), (void*) hostB);
  queue.finish();
}

//-----------------------------------------------------------------------------
void enqueReadCommands(Queue& queue) {
  queue.readBuffer(*C, SIZE * SIZE * sizeof(float), (void*) hostC);
  queue.finish();
}

//-----------------------------------------------------------------------------
void setKernelArguments() {
  kernel->setArgument(0, *A);
  kernel->setArgument(1, *B);
  kernel->setArgument(2, *C);
  kernel->setArgument(3, sizeof(cl_int), (void*) width);
  kernel->setArgument(4, sizeof(cl_int), (void*) height);
}

//-----------------------------------------------------------------------------
void run(const Context* context, Queue& queue) {
  long executionTime = 0l;
  for (unsigned int repetition = 0; repetition < REPETITIONS; ++repetition) {
    Event runEvent(*context);
    queue.run(*kernel, DIMENSIONS, 0, globalWorkSize, localWorkSize, runEvent);
    queue.finish();
    executionTime += runEvent.computeDuration();
  }
  std::cout << executionTime << "\n";
}

//-----------------------------------------------------------------------------
void verifyResults() {
  float* cpuHostC = new float [SIZE * SIZE];
  for(unsigned int row = 0; row < SIZE; ++row) {
    for(unsigned int column = 0; column < SIZE; ++column) {
      float result = 0.0f;
      for(unsigned int index = 0; index < SIZE; ++index) 
        result += hostA[row * SIZE + index] * hostB[index * SIZE + column];
      if(abs(hostC[row * SIZE + column] - result) >= 0.001f)
        std::cout << "Error\n";
      cpuHostC[row * SIZE + column] = result;
    }
  }
  delete [] cpuHostC;
}

//-----------------------------------------------------------------------------
float random(float rand_min, float rand_max) {
  float result =(float)rand()/(float)RAND_MAX;
  return ((1.0 - result) * rand_min + result *rand_max);
}

//-----------------------------------------------------------------------------
void printVector(const float* printVector, unsigned int size) {
  for (unsigned int index = 0; index < size; ++index) {
    std::cout << printVector[index] << " ";
  }
  std::cout << std::endl;
}

//-----------------------------------------------------------------------------
void printMatrix(const float* matrix) {
  for (unsigned int row = 0; row < SIZE; ++row) {
    for (unsigned int column = 0; column < SIZE; ++column) {
      std::cout << matrix[row * SIZE + column] << " ";
    }
    std::cout << std::endl;
  }
}
