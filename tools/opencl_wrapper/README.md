OpenCLWrapper
=============

Requirements
------------

OpenCLWrapper requires OpenCL.
It relies on CMake. Out of tree compilation is suggested.

Before compiling modify the main "CMakeLists.txt" adding the 
right paths to the path lists: OPENCL_INCLUDE_PATHS, OPENCL_LIB_PATHS.

The "examples" directory contains the client code for 
matrix multiplication algorithm.

Author
-------

For any questions contact [Alberto Magni][1]

[1]: mailto:alberto.magni86@gmail.com
