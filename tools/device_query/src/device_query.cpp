#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <CL/cl.h>

#define temp_buf_size 1024

const char *cl_err2str[] = {
    "CL_SUCCESS",
    "CL_DEVICE_NOT_FOUND",                          // -1
    "CL_DEVICE_NOT_AVAILABLE",                      // -2
    "CL_COMPILER_NOT_AVAILABLE",                    // -3
    "CL_MEM_OBJECT_ALLOCATION_FAILURE",             // -4
    "CL_OUT_OF_RESOURCES",                          // -5
    "CL_OUT_OF_HOST_MEMORY",                        // -6
    "CL_PROFILING_INFO_NOT_AVAILABLE",              // -7
    "CL_MEM_COPY_OVERLAP",                          // -8
    "CL_IMAGE_FORMAT_MISMATCH",                     // -9
    "CL_IMAGE_FORMAT_NOT_SUPPORTED",                // -10
    "CL_BUILD_PROGRAM_FAILURE",                     // -11
    "CL_MAP_FAILURE",                               // -12
    "CL_MISALIGNED_SUB_BUFFER_OFFSET",              // -13
    "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST", // -14
    "Unknown Error -15",
    "Unknown Error -16",
    "Unknown Error -17",
    "Unknown Error -18",
    "Unknown Error -19",
    "Unknown Error -20",
    "Unknown Error -21",
    "Unknown Error -22",
    "Unknown Error -23",
    "Unknown Error -24",
    "Unknown Error -25",
    "Unknown Error -26",
    "Unknown Error -27",
    "Unknown Error -28",
    "Unknown Error -29",
    "CL_INVALID_VALUE",                             // -30
    "CL_INVALID_DEVICE_TYPE",                       // -31
    "CL_INVALID_PLATFORM",                          // -32
    "CL_INVALID_DEVICE",                            // -33
    "CL_INVALID_CONTEXT",                           // -34
    "CL_INVALID_QUEUE_PROPERTIES",                  // -35
    "CL_INVALID_COMMAND_QUEUE",                     // -36
    "CL_INVALID_HOST_PTR",                          // -37
    "CL_INVALID_MEM_OBJECT",                        // -38
    "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",           // -39
    "CL_INVALID_IMAGE_SIZE",                        // -40
    "CL_INVALID_SAMPLER",                           // -41
    "CL_INVALID_BINARY",                            // -42
    "CL_INVALID_BUILD_OPTIONS",                     // -43
    "CL_INVALID_PROGRAM",                           // -44
    "CL_INVALID_PROGRAM_EXECUTABLE",                // -45
    "CL_INVALID_KERNEL_NAME",                       // -46
    "CL_INVALID_KERNEL_DEFINITION",                 // -47
    "CL_INVALID_KERNEL",                            // -48
    "CL_INVALID_ARG_INDEX",                         // -49
    "CL_INVALID_ARG_VALUE",                         // -50
    "CL_INVALID_ARG_SIZE",                          // -51
    "CL_INVALID_KERNEL_ARGS",                       // -52
    "CL_INVALID_WORK_DIMENSION",                    // -53
    "CL_INVALID_WORK_GROUP_SIZE",                   // -54
    "CL_INVALID_WORK_ITEM_SIZE",                    // -55
    "CL_INVALID_GLOBAL_OFFSET",                     // -56
    "CL_INVALID_EVENT_WAIT_LIST",                   // -57
    "CL_INVALID_EVENT",                             // -58
    "CL_INVALID_OPERATION",                         // -59
    "CL_INVALID_GL_OBJECT",                         // -60
    "CL_INVALID_BUFFER_SIZE",                       // -61
    "CL_INVALID_MIP_LEVEL",                         // -62
    "CL_INVALID_GLOBAL_WORK_SIZE",                  // -63
    "CL_INVALID_PROPERTY"                           // -64
};

void checkCLcall(int err) {
    if(err == CL_SUCCESS)
	return;
    
    if((err > 0) || (err <-64)) {
	fprintf(stderr, "Unknown OpenCL error code! (%d)\n", err);
    } else {
	fprintf(stderr, "OpenCL error: %s! (%d)\n", cl_err2str[-err], err);
    }
    abort();
}

int main(int argc, char **argv) {
	cl_platform_id platform_id[256];
	cl_uint num_platforms;
	int i;

	checkCLcall(clGetPlatformIDs(256, &platform_id[0], &num_platforms));

	printf("Number of OpenCL Platforms: %d\n", num_platforms);

	for (i=0; i<num_platforms; i++) {
		printf("OpenCL Platform %d:\n", i);
		char temp_buf[temp_buf_size+1];
		checkCLcall(clGetPlatformInfo(platform_id[i],
					      CL_PLATFORM_PROFILE,
					      temp_buf_size,
					      temp_buf,
					      NULL));
		printf("CL_PLATFORM_PROFILE = %s\n", temp_buf);

		checkCLcall(clGetPlatformInfo(platform_id[i],
					      CL_PLATFORM_VERSION,
					      temp_buf_size,
					      temp_buf,
					      NULL));
		printf("CL_PLATFORM_VERSION = %s\n", temp_buf);
		
		checkCLcall(clGetPlatformInfo(platform_id[i],
					      CL_PLATFORM_NAME,
					      temp_buf_size,
					      temp_buf,
					      NULL));
		printf("CL_PLATFORM_NAME = %s\n", temp_buf);

		checkCLcall(clGetPlatformInfo(platform_id[i],
					      CL_PLATFORM_VENDOR,
					      temp_buf_size,
					      temp_buf,
					      NULL));
		printf("CL_PLATFORM_VENDOR = %s\n", temp_buf);

		checkCLcall(clGetPlatformInfo(platform_id[i],
					      CL_PLATFORM_EXTENSIONS,
					      temp_buf_size,
					      temp_buf,
					      NULL));
		printf("CL_PLATFORM_EXTENSIONS = %s\n", temp_buf);

		cl_device_id devices[255];
		cl_uint num_devices;
		int j;
		
		checkCLcall(clGetDeviceIDs(platform_id[i], CL_DEVICE_TYPE_ALL,
					   255, &devices[0], &num_devices));
		printf("Number of OpenCL Devices for Platform %d: %d\n",
			   i, num_devices);
		for (j=0; j < num_devices; j++) {
			cl_device_type device_type;
			cl_uint uint_temp;
			cl_ulong ulong_temp;
			cl_bool  bool_temp;
			cl_device_fp_config device_fp_config;
			cl_device_mem_cache_type device_mem_cache_type;
			cl_device_local_mem_type device_local_mem_type;
			cl_device_exec_capabilities device_exec_capabilities;
			cl_command_queue_properties command_queue_properties;
			cl_uint cl_version;
			int cl_version_maj, cl_version_min;
			size_t *item_sizes, size_t_temp;
			int k;
			
			printf("Device %d:\n", j);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_VERSION,
								temp_buf_size,
								temp_buf,
								NULL));
			assert(sscanf(temp_buf, "OpenCL %d.%d", &cl_version_maj, &cl_version_min) == 2);
			assert(cl_version_maj < 0xffff);
			assert(cl_version_min < 0xffff);
			cl_version = (cl_version_maj << 16) | cl_version_min;

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_TYPE,
								sizeof(cl_device_type),
								&device_type,
								NULL));
			printf("  CL_DEVICE_TYPE =%s%s%s%s\n",
				   (device_type & CL_DEVICE_TYPE_CPU)?" CL_DEVICE_TYPE_CPU":"",
				   (device_type & CL_DEVICE_TYPE_GPU)?" CL_DEVICE_TYPE_GPU":"",
				   (device_type & CL_DEVICE_TYPE_ACCELERATOR)?" CL_DEVICE_TYPE_ACCELERATOR":"",
				   (device_type & CL_DEVICE_TYPE_DEFAULT)?" CL_DEVICE_TYPE_DEFAULT":"");
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_VENDOR_ID,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_VENDOR_ID = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_COMPUTE_UNITS,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_MAX_COMPUTE_UNITS = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS = %d\n", uint_temp);
			
			item_sizes = (size_t*) malloc(uint_temp * sizeof(size_t));
			assert(item_sizes != NULL);
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_WORK_ITEM_SIZES,
								sizeof(size_t)*uint_temp,
								item_sizes,
								NULL));
			printf("  CL_DEVICE_MAX_WORK_ITEM_SIZES = (");
			for (k=0; k<uint_temp; k++) {
				printf("%lu%s", item_sizes[k], (k == uint_temp-1)?")\n":", ");
			}
			free(item_sizes);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_WORK_GROUP_SIZE,
								sizeof(size_t_temp),
								&size_t_temp,
								NULL));
			printf("  CL_DEVICE_MAX_WORK_GROUP_SIZE = %lu\n", size_t_temp);

//			checkCLcall(clGetDeviceInfo(devices[j], CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
//								sizeof(size_t_temp),
//								&size_t_temp,
//								NULL));
//			printf("  CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE = %lu\n", size_t_temp);


			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE = %d\n", uint_temp);
			
			#ifdef CL_VERSION_1_1
			if(cl_version >= 0x00010001){
			    checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF,
								    sizeof(cl_uint),
								    &uint_temp,
								    NULL));
			    printf("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF = %d\n", uint_temp);
			    
			    checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR,
								    sizeof(cl_uint),
								    &uint_temp,
								    NULL));
			    printf("  CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR = %d\n", uint_temp);
			    
			    checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT,
								    sizeof(cl_uint),
								    &uint_temp,
								    NULL));
			    printf("  CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT = %d\n", uint_temp);
			    
			    checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_NATIVE_VECTOR_WIDTH_INT,
								    sizeof(cl_uint),
								    &uint_temp,
								    NULL));
			    printf("  CL_DEVICE_NATIVE_VECTOR_WIDTH_INT = %d\n", uint_temp);
			    
			    checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG,
								    sizeof(cl_uint),
								    &uint_temp,
								    NULL));
			    printf("  CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG = %d\n", uint_temp);
			    
			    checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT,
								    sizeof(cl_uint),
								    &uint_temp,
								    NULL));
			    printf("  CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT = %d\n", uint_temp);
			    
			    checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE,
								    sizeof(cl_uint),
								    &uint_temp,
								    NULL));
			    printf("  CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE = %d\n", uint_temp);
			    
			    checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF,
								    sizeof(cl_uint),
								    &uint_temp,
								    NULL));
			    printf("  CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF = %d\n", uint_temp);
			}
			#endif
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_CLOCK_FREQUENCY,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_MAX_CLOCK_FREQUENCY = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_ADDRESS_BITS,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_ADDRESS_BITS = %d\n", uint_temp);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_MEM_ALLOC_SIZE,
								sizeof(cl_ulong),
								&ulong_temp,
								NULL));
			printf("  CL_DEVICE_MAX_MEM_ALLOC_SIZE = %lu\n", ulong_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_IMAGE_SUPPORT,
								sizeof(cl_bool),
								&bool_temp,
								NULL));
			printf("  CL_DEVICE_IMAGE_SUPPORT = %s\n",
				   (bool_temp == CL_TRUE)?"true":"false");
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_READ_IMAGE_ARGS,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_MAX_READ_IMAGE_ARGS = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_WRITE_IMAGE_ARGS,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_MAX_WRITE_IMAGE_ARGS = %d\n", uint_temp);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_IMAGE2D_MAX_WIDTH,
								sizeof(size_t_temp),
								&size_t_temp,
								NULL));
			printf("  CL_DEVICE_IMAGE2D_MAX_WIDTH = %lu\n", size_t_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_IMAGE2D_MAX_HEIGHT,
								sizeof(size_t_temp),
								&size_t_temp,
								NULL));
			printf("  CL_DEVICE_IMAGE2D_MAX_HEIGHT = %lu\n", size_t_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_IMAGE3D_MAX_WIDTH,
								sizeof(size_t_temp),
								&size_t_temp,
								NULL));
			printf("  CL_DEVICE_IMAGE3D_MAX_WIDTH = %lu\n", size_t_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_IMAGE3D_MAX_HEIGHT,
								sizeof(size_t_temp),
								&size_t_temp,
								NULL));
			printf("  CL_DEVICE_IMAGE3D_MAX_HEIGHT = %lu\n", size_t_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_IMAGE3D_MAX_DEPTH,
								sizeof(size_t_temp),
								&size_t_temp,
								NULL));
			printf("  CL_DEVICE_IMAGE3D_MAX_DEPTH = %lu\n", size_t_temp);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_SAMPLERS,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_MAX_SAMPLERS = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_PARAMETER_SIZE,
								sizeof(size_t_temp),
								&size_t_temp,
								NULL));
			printf("  CL_DEVICE_MAX_PARAMETER_SIZE = %lu\n", size_t_temp);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MEM_BASE_ADDR_ALIGN,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_MEM_BASE_ADDR_ALIGN = %d\n", uint_temp);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE = %d\n", uint_temp);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_SINGLE_FP_CONFIG,
								sizeof(cl_device_fp_config),
								&device_fp_config,
								NULL));
			printf("  CL_DEVICE_SINGLE_FP_CONFIG =%s%s%s%s%s%s\n",
				   (device_fp_config & CL_FP_DENORM)?" CL_FP_DENORM":"",
				   (device_fp_config & CL_FP_INF_NAN)?" CL_FP_INF_NAN":"",
				   (device_fp_config & CL_FP_ROUND_TO_NEAREST)?" CL_FP_ROUND_TO_NEAREST":"",
				   (device_fp_config & CL_FP_ROUND_TO_ZERO)?" CL_FP_ROUND_TO_ZERO":"",
				   (device_fp_config & CL_FP_ROUND_TO_INF)?" CL_FP_ROUND_TO_INF":"",
				   (device_fp_config & CL_FP_FMA)?" CL_FP_FMA":"",
			#ifdef CL_VERSION_1_1
				   (device_fp_config & CL_FP_SOFT_FLOAT)?" CL_FP_SOFT_FLOAT":""
			#else
				   ""
			#endif
			       );
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_GLOBAL_MEM_CACHE_TYPE,
								sizeof(cl_device_mem_cache_type),
								&device_mem_cache_type,
								NULL));
			printf("  CL_DEVICE_GLOBAL_MEM_CACHE_TYPE = ");
			switch (device_mem_cache_type) {
				case CL_NONE:
					printf("CL_NONE\n");
					break;
				case CL_READ_ONLY_CACHE:
					printf("CL_READ_ONLY_CACHE\n");
					break;
				case CL_READ_WRITE_CACHE:
					printf("CL_READ_WRITE_CACHE\n");
					break;
				default:
					printf("\n");
					break;
			}

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
								sizeof(cl_ulong),
								&ulong_temp,
								NULL));
			printf("  CL_DEVICE_GLOBAL_MEM_CACHE_SIZE = %lu\n", ulong_temp);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_GLOBAL_MEM_SIZE,
								sizeof(cl_ulong),
								&ulong_temp,
								NULL));
			printf("  CL_DEVICE_GLOBAL_MEM_SIZE = %lu\n", ulong_temp);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,
								sizeof(cl_ulong),
								&ulong_temp,
								NULL));
			printf("  CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE = %lu\n", ulong_temp);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_MAX_CONSTANT_ARGS,
								sizeof(cl_uint),
								&uint_temp,
								NULL));
			printf("  CL_DEVICE_MAX_CONSTANT_ARGS = %d\n", uint_temp);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_LOCAL_MEM_TYPE,
								sizeof(cl_device_local_mem_type),
								&device_local_mem_type,
								NULL));
			printf("  CL_DEVICE_LOCAL_MEM_TYPE = ");
			switch (device_local_mem_type) {
				case CL_LOCAL:
					printf("CL_LOCAL\n");
					break;
				case CL_GLOBAL:
					printf("CL_GLOBAL\n");
					break;
				default:
					printf("\n");
					break;
			}
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_LOCAL_MEM_SIZE,
								sizeof(cl_ulong),
								&ulong_temp,
								NULL));
			printf("  CL_DEVICE_LOCAL_MEM_SIZE = %lu\n", ulong_temp);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_ERROR_CORRECTION_SUPPORT,
								sizeof(cl_bool),
								&bool_temp,
								NULL));
			printf("  CL_DEVICE_ERROR_CORRECTION_SUPPORT = %s\n",
				   (bool_temp == CL_TRUE)?"true":"false");
			
			#ifdef CL_VERSION_1_1
			if(cl_version >= 0x00010001){
			    checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_HOST_UNIFIED_MEMORY,
								    sizeof(cl_bool),
								    &bool_temp,
								    NULL));
			    printf("  CL_DEVICE_HOST_UNIFIED_MEMORY = %s\n",
				    (bool_temp == CL_TRUE)?"true":"false");
			}
			#endif

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_PROFILING_TIMER_RESOLUTION,
								sizeof(size_t_temp),
								&size_t_temp,
								NULL));
			printf("  CL_DEVICE_PROFILING_TIMER_RESOLUTION = %lu\n", size_t_temp);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_ENDIAN_LITTLE,
								sizeof(cl_bool),
								&bool_temp,
								NULL));
			printf("  CL_DEVICE_ENDIAN_LITTLE = %s\n",
				   (bool_temp == CL_TRUE)?"true":"false");

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_AVAILABLE,
								sizeof(cl_bool),
								&bool_temp,
								NULL));
			printf("  CL_DEVICE_AVAILABLE = %s\n",
				   (bool_temp == CL_TRUE)?"true":"false");

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_COMPILER_AVAILABLE,
								sizeof(cl_bool),
								&bool_temp,
								NULL));
			printf("  CL_DEVICE_COMPILER_AVAILABLE = %s\n",
				   (bool_temp == CL_TRUE)?"true":"false");
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_EXECUTION_CAPABILITIES,
								sizeof(cl_device_exec_capabilities),
								&device_exec_capabilities,
								NULL));
			printf("  CL_DEVICE_EXECUTION_CAPABILITIES =%s%s\n",
				   (device_exec_capabilities & CL_EXEC_KERNEL)?" CL_EXEC_KERNEL":"",
				   (device_exec_capabilities & CL_EXEC_NATIVE_KERNEL)?" CL_EXEC_NATIVE_KERNEL":"");
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_QUEUE_PROPERTIES,
								sizeof(cl_command_queue_properties),
								&command_queue_properties,
								NULL));
			printf("  CL_DEVICE_QUEUE_PROPERTIES =%s%s\n",
				   (command_queue_properties & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE)?" CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE":"",
				   (command_queue_properties & CL_QUEUE_PROFILING_ENABLE)?" CL_QUEUE_PROFILING_ENABLE":"");
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_NAME,
								temp_buf_size,
								temp_buf,
								NULL));
			printf("  CL_DEVICE_NAME = %s\n", temp_buf);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_VENDOR,
								temp_buf_size,
								temp_buf,
								NULL));
			printf("  CL_DEVICE_VENDOR = %s\n", temp_buf);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DRIVER_VERSION,
								temp_buf_size,
								temp_buf,
								NULL));
			printf("  CL_DRIVER_VERSION = %s\n", temp_buf);

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_PROFILE,
								temp_buf_size,
								temp_buf,
								NULL));
			printf("  CL_DEVICE_PROFILE = %s\n", temp_buf);
			
			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_VERSION,
								temp_buf_size,
								temp_buf,
								NULL));
			printf("  CL_DEVICE_VERSION = %s\n", temp_buf);

			#ifdef CL_VERSION_1_1
			if(cl_version >= 0x00010001){
			    checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION,
								    temp_buf_size,
								    temp_buf,
								    NULL));
			    printf("  CL_DEVICE_OPENCL_C_VERSION = %s\n", temp_buf);
			}
			#endif

			checkCLcall(clGetDeviceInfo(devices[j], CL_DEVICE_EXTENSIONS,
								temp_buf_size,
								temp_buf,
								NULL));
			printf("  CL_DEVICE_EXTENSIONS = %s\n", temp_buf);

		}
	}
	
	return 0;
}
