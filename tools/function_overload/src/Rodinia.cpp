#include <CL/cl.h>

#include "Utils.h"

#include <stdlib.h>
#include <sys/types.h>
#include <dlfcn.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>

int compileAxtor(std::string &inputFile, std::string &options, std::string &outputFile, int seed);
int compilePTX(std::string &inputFile, std::string &options, std::string &outputFile);

extern "C" cl_program clCreateProgramWithSource(
                      cl_context context, cl_uint count, const char** strings,
                      const size_t *length, cl_int* errcode_ret) {
  srand((unsigned)time(0));
  int seed = rand() % 100000; 

  std::cout << "HIJACKED_AXTOR clCreateProgramWithSource HIJACKED_AXTOR\n";

  // Get pointers to original function calls.
  clCreateProgramWithSourceFunction originalCreateProgramWithSource;
  *(void **)(&originalCreateProgramWithSource) = 
    dlsym(RTLD_NEXT, CL_CREATE_PROGRAM_WITH_SOURCE_NAME);

  cl_program tmpProgram = originalCreateProgramWithSource(
                          context, count, strings, length, errcode_ret);
  verifyOutputCode(*errcode_ret, "Error creating the program with source");

  std::string compiler = getEnvString(OCL_COMPILER);
  if(compiler == "")
    return tmpProgram;

  std::stringstream oclStream;
  std::stringstream outputStream;
    
  oclStream << OCL_INPUT_FILE << seed;
  outputStream << OCL_OUTPUT_FILE << seed;

  std::string oclFile = oclStream.str();
  std::string outputFile = outputStream.str();

  // Get the program source code.
  size_t codeSize;
  char* sourceProgram = getProgramSourceCode(tmpProgram, &codeSize);
  std::string sourceProgramString(sourceProgram); 

  // Dump the source code to /tmp/ocl.cl
  writeFile(oclFile, sourceProgramString);

  // Invoke the external compiler. The ptx file will be placed into 
  // /tmp/tmp.ptx
  std::string options = getEnvString(OCL_COMPILER_OPTIONS);
  
  int compileResult = compilePTX(oclFile, options, outputFile);
  if(compileResult != 0) {
    std::cout << "Error compiling the program: " << compileResult << 
                 "\nEXITING\n";
    exit(1);
  }

  size_t newSourceSize;
  const char* sourceCode = readFile(outputFile.c_str(), &newSourceSize);

  // Get the list of devices associated with the program.
  unsigned int devicesNumber;
  cl_device_id* devices = getProgramDevices(tmpProgram, &devicesNumber);

  // Create a new program from source.
  cl_int errorCode;
  cl_program newProgram = originalCreateProgramWithSource(context, 1, &sourceCode, 
                                                          &newSourceSize, &errorCode);
  verifyOutputCode(errorCode, "Error creating the new program");

  std::string remove = "rm " + oclFile + " && rm " + outputFile;
  system(remove.c_str());

  delete [] devices;
  delete [] sourceProgram;

  return newProgram;
}

//------------------------------------------------------------------------------
int compileAxtor(std::string &inputFile, std::string &options, 
                 std::string &outputFile, 
                 int seed) {

  std::stringstream bitcodeStream;
  bitcodeStream << BC_FILE << seed; 
  std::string bitcodeFile = bitcodeStream.str();

  // Inline commands.
  std::string sedCmdOne = "sed \'s/__inline/inline/\' -i " + inputFile;
  std::string sedCmdTwo = "sed \'s/inline/static inline/\' -i " + inputFile;

  // Compilation options.
  std::string axtorBuildOptions = getEnvString("AXTOR_CLANG_OPTIONS", "-O0");
  std::string axtorOptOptions = getEnvString("AXTOR_OPT_OPTIONS");
  
  std::string optBin = "opt";
  std::string axtorBin = "axtor";

  // Clang command.
  std::string clangCmd = "LD_PRELOAD=\"\" clang -x cl -target ptx32 -include " 
                         OCL_HEADER_PATH " " + 
//                         inputFile + " -S -emit-llvm -o " + bitcodeFile + " 2> /dev/null";
                         inputFile + " -S -emit-llvm -o " + bitcodeFile;

  // Opt command.
  std::stringstream optCmdOut;
  optCmdOut << "LD_PRELOAD=\"\" "
            << optBin << " "
            << axtorOptOptions         // specified by AXTOR_OPT_OPTIONS
            << " " << bitcodeFile << " -o " << bitcodeFile;

  std::string optCmd = optCmdOut.str();
  std::string axtorCmd = "LD_PRELOAD=\"\" " + axtorBin + " " + bitcodeFile
                         + " -m OCL " + "-o " + outputFile + " 2> /dev/null";
  std::cout << "CLANG:\n" << clangCmd << 
               "\nOPT:\n" << optCmd <<
               "\nAXTOR:\n" << axtorCmd << "\n";

  // Inline.
  system(sedCmdOne.c_str());
  system(sedCmdTwo.c_str());

  // Clang.
  if (system(clangCmd.c_str())) {
    std::cout << "&&&&& FRONTEND_FAILURE!";
    return 1;
  }

  // Opt.
  if (system(optCmd.c_str())) {
    std::cout << "&&&&& OPT_FAILURE!";
    return 2;
  }

  // Axtor.
  if (system(axtorCmd.c_str())) {
    std::cout << "&&&&& AXTOR_FAILURE!";
    return 3;
  }

  std::string remove = "rm " + bitcodeFile;
  system(remove.c_str());

  return 0;
}

//------------------------------------------------------------------------------
int compilePTX(std::string &inputFile, std::string &options,
               std::string &outputFile) {
  std::string ptxString = buildPTXCommandLine(inputFile, options, outputFile);
  std::cout << ptxString << "\n";
  if(system(ptxString.c_str()) != 0) {
    std::cout << "Error compiling the program for PTX.\nEXITING\n";
    exit(1);
  }
}
