#include <CL/cl.h>

#include "Utils.h"

#include <stdlib.h>
#include <sys/types.h>
#include <dlfcn.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>

extern "C" cl_program clCreateProgramWithSource(
                      cl_context context, cl_uint count, const char** strings,
                      const size_t *length, cl_int* errcode_ret) {
  srand((unsigned)time(0));
  int seed = rand() % 100000; 

  std::cout << "HIJACKED_PTX clCreateProgramWithSource HIJACKED_PTX\n";

  // Get pointers to original function calls.
  clCreateProgramWithSourceFunction originalCreateProgramWithSource;
  *(void **)(&originalCreateProgramWithSource) = 
    dlsym(RTLD_NEXT, CL_CREATE_PROGRAM_WITH_SOURCE_NAME);

  cl_program tmpProgram = originalCreateProgramWithSource(
                          context, count, strings, length, errcode_ret);
  verifyOutputCode(*errcode_ret, "Error creating the program with source");

  std::stringstream oclStream;
  std::stringstream ptxStream;
    
  oclStream << OCL_INPUT_FILE << seed;
  ptxStream << PTX_FILE << seed;

  std::string oclFile = oclStream.str();
  std::string ptxFile = ptxStream.str();

  // Get program devices.
  unsigned int devicesNumber;  
  cl_device_id* devices = getProgramDevices(tmpProgram, &devicesNumber);

  // Get the program source code.
  size_t codeSize;
  char* sourceProgram = getProgramSourceCode(tmpProgram, &codeSize);
  std::string sourceProgramString (sourceProgram); 

  // Dump the source code to /tmp/ocl.cl
  writeFile(oclFile, sourceProgramString);

  // Invoke the external compiler. The ptx file will be placed into 
  // /tmp/tmp.ptx
  std::string options = getEnvString(OCL_COMPILER_OPTIONS);
  std::string compilerString = buildPTXCommandLine(oclFile, options, ptxFile);
  std::cout << compilerString << "\n";
  if(system(compilerString.c_str()) != 0) {
    std::cout << "Error compiling the program for PTX.\nEXITING\n";
    exit(1);
  }

  size_t binarySize;
  const char* binaryCode = readFile(ptxFile.c_str(), &binarySize);

  // Create a new program with binary.
  int binaryStatus;
  int errorCode;
  cl_program newProgram = clCreateProgramWithBinary(
                          context, devicesNumber, devices,
                          &binarySize, (const unsigned char **) &binaryCode,
                          &binaryStatus, &errorCode);

  delete [] devices;
  delete [] sourceProgram;

  std::string removeString = "rm " + oclFile + " && rm " + ptxFile;
  system(removeString.c_str());

  return newProgram;
}
