/*#include <CL/cl.h>

#include <sys/types.h>
#include <dlfcn.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>

#define FUNCTION_NAME "clCreateProgramWithSource"
#define OPENCL_SOURCE_CODE "OPENCL_SOURCE_CODE"

char* readFile(const char* filePath, size_t* size);

cl_program clCreateProgramWithSource(cl_context context,
                                     cl_uint count,
                                     const char **strings,
                                     const size_t *lengths,
                                     cl_int *errcode_ret) {
  std::cout << "PIPPO clCreateProgramWithSource PIPPO\n";

  // Get the original function.
  cl_program (*original_clCreateProgramWithSource)
             (cl_context context,
              cl_uint count,
              const char **strings,
              const size_t *lengths,
              cl_int *errcode_ret);
  *(void **)(&original_clCreateProgramWithSource) = 
    dlsym(RTLD_NEXT, FUNCTION_NAME);

  size_t length;
  char* sourceCodeFile = getenv(OPENCL_SOURCE_CODE);
  if(sourceCodeFile == NULL)
    throw std::runtime_error("No OPENCL_SOURCE_CODE variable set!");
  char* sourceProgram = readFile(sourceCodeFile, &length); 

  // Invoke the function.
  cl_program program = original_clCreateProgramWithSource(
                       context, count, (const char **)&sourceProgram, 
                       &length, errcode_ret);
  return program;
}

char* readFile(const char* filePath, size_t* size) {
  std::ifstream fileStream(filePath);
  if(fileStream.is_open()) {
    fileStream.seekg(0, std::ios::end);
    int fileSize = fileStream.tellg();
    char* source = new char [fileSize + 1];
    fileStream.seekg(0, std::ios::beg);
    fileStream.read(source, fileSize);
    fileStream.close();
    source[fileSize] = '\0';
    *size = fileSize + 1;
    return source;
  }
  else {
    std::stringstream ss;
    ss << "Cannot open: " << filePath;
    throw std::runtime_error(ss.str());
  }
}*/
