#include <CL/cl.h>

#include "Utils.h"

#include <stdlib.h>
#include <sys/types.h>
#include <dlfcn.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>

#define OCL_FILE "/tmp/ocl.cl"
#define OUTPUT_FILE "/tmp/output.cl"
#define BC_FILE "/tmp/bc.ll"

int compile(std::string &inputFile, std::string &options, std::string &outputFile, int seed);

extern "C" cl_program clCreateProgramWithSource(
                      cl_context context, cl_uint count, const char** strings,
                      const size_t *length, cl_int* errcode_ret) {
  srand((unsigned)time(0));
  int seed = rand() % 100000; 

  std::cout << "HIJACKED_AXTOR clCreateProgramWithSource HIJACKED_AXTOR\n";

  // Get pointers to original function calls.
  clCreateProgramWithSourceFunction originalCreateProgramWithSource;
  *(void **)(&originalCreateProgramWithSource) = 
    dlsym(RTLD_NEXT, CL_CREATE_PROGRAM_WITH_SOURCE_NAME);

  std::stringstream oclStream;
  std::stringstream outputStream;
    
  oclStream << OCL_FILE << seed;
  outputStream << OUTPUT_FILE << seed;

  std::string oclFile = oclStream.str();
  std::string outputFile = outputStream.str();

  // Get the program source code.
  size_t codeSize;
  if(count != 1)
    std::cout << "String list size not one!\n";

  const char* sourceProgram = strings[0];
  std::string sourceProgramString(sourceProgram); 

  // Dump the source code to /tmp/ocl.cl
  writeFile(oclFile, sourceProgramString);

  // Invoke the external compiler. The ptx file will be placed into 
  // /tmp/tmp.ptx
  std::string options = getEnvString(CLASH_COMPILER_OPTIONS);
  
  int compileResult = compile(oclFile, options, outputFile, seed);
  if(compileResult != 0) {
    std::cout << "Error compiling the program: " << compileResult << 
                 "\nEXITING\n";
    exit(1);
  }

  size_t newSourceSize;
  const char* sourceCode = readFile(outputFile.c_str(), &newSourceSize);

  // Create a new program from source.
  cl_int errorCode;
  cl_program newProgram = originalCreateProgramWithSource(context, 1, &sourceCode, 
                                                          &newSourceSize, &errorCode);
  verifyOutputCode(errorCode, "Error creating the new program");

  std::string remove = "rm " + oclFile + " && rm " + outputFile;
  system(remove.c_str());

  //delete [] sourceProgram;

  std::cout << "Program creation overload finished\n";

  *errcode_ret = CL_SUCCESS;
  return newProgram;
}

//------------------------------------------------------------------------------
int compile(std::string &inputFile, std::string &options, 
            std::string &outputFile, 
            int seed) {

  std::stringstream bitcodeStream;
  bitcodeStream << BC_FILE << seed; 
  std::string bitcodeFile = bitcodeStream.str();

  // Inline commands.
  std::string sedCmdOne = "sed \'s/__inline/inline/\' -i " + inputFile;
  std::string sedCmdTwo = "sed \'s/inline/static inline/\' -i " + inputFile;

  // Compilation options.
  std::string axtorBuildOptions = getEnvString("AXTOR_CLANG_OPTIONS", "-O0");
  std::string axtorOptOptions = getEnvString("AXTOR_OPT_OPTIONS");
  
  std::string optBin = "opt";
  std::string axtorBin = "axtor";

  // Clang command.
  std::string clangCmd = "LD_PRELOAD=\"\" clang -x cl -target ptx32 -include " 
                         OCL_HEADER_PATH " -O0 " + 
                         inputFile + " -S -emit-llvm -fno-builtin -o " + 
//                         bitcodeFile + " 2> /dev/null";
                         bitcodeFile + " 2> /dev/null";

  // Opt command.
  std::stringstream optCmdOut;
  optCmdOut << "LD_PRELOAD=\"\" "
            << optBin << " "
            << options         // specified by AXTOR_OPT_OPTIONS
            << " " << bitcodeFile << " -o " << bitcodeFile;

  std::string optCmd = optCmdOut.str();
  std::string axtorCmd = "LD_PRELOAD=\"\" " + axtorBin + " " + bitcodeFile
                         + " -m OCL " + "-o " + outputFile + " 2> /dev/null";
  std::cout << "CLANG:\n" << clangCmd << 
               "\nOPT:\n" << optCmd <<
               "\nAXTOR:\n" << axtorCmd << "\n";

  // Inline.
  system(sedCmdOne.c_str());
  system(sedCmdTwo.c_str());

  // Clang.
  if (system(clangCmd.c_str())) {
    std::cout << "&&&&& FRONTEND_FAILURE!";
    return 1;
  }

  // Opt.
  if (system(optCmd.c_str())) {
    std::cout << "&&&&& OPT_FAILURE!";
    return 2;
  }

  // Axtor.
  if (system(axtorCmd.c_str())) {
    std::cout << "&&&&& AXTOR_FAILURE!";
    return 3;
  }

  std::string remove = "rm " + bitcodeFile;
  system(remove.c_str());

  return 0;
}

//------------------------------------------------------------------------------
cl_int clEnqueueNDRangeKernel(cl_command_queue command_queue,
                              cl_kernel kernel,
                              cl_uint work_dim,
                              const size_t* global_work_offset,
                              const size_t* global_work_size,
                              const size_t* local_work_size,
                              cl_uint num_events_in_wait_list,
                              const cl_event* event_wait_list,
                              cl_event* event) {
  std::cout << "HIJACKED clEnqueueNDRangeKernel HIJACKED\n";
  std::string CFString = getEnvString(CLASH_CF);

  // Get pointer to original function calls.
  clEnqueueNDRangeKernelFunction originalclEnqueueKernel;
    *(void **)(&originalclEnqueueKernel) =
    dlsym(RTLD_NEXT, CL_ENQUEUE_NDRANGE_KERNEL_NAME);


  // Check if there is something to do.
  if (CFString == "")
    return originalclEnqueueKernel(command_queue, kernel, work_dim,
                                   global_work_offset, global_work_size,
                                   local_work_size, num_events_in_wait_list,
                                   event_wait_list, event);

  std::string CDString = getEnvString(CLASH_CD);

  size_t* newGlobalSize = new size_t [work_dim];
  size_t* newLocalSize = new size_t [work_dim];

  int CF, CD = 0;
  std::stringstream(CDString) >> CD;
  std::stringstream(CFString) >> CF;

  std::cout << "CD: " << CD << " CF: " << CF << "\n";

  // Reduce the size of the NDRange space.
  for (unsigned int index = 0; index < work_dim; ++index) {
    if(index == CD) {
      newGlobalSize[index] = global_work_size[index] / CF;
      newLocalSize[index] = local_work_size[index] / CF;
    }
    else {
      newGlobalSize[index] = global_work_size[index];
      newLocalSize[index] = local_work_size[index];
    }
    std::cout << "GS[" << index << "] = " << newGlobalSize[index] << "\n";
    std::cout << "LS[" << index << "] = " << newLocalSize[index] << "\n";
  }

  return originalclEnqueueKernel(command_queue, kernel, work_dim,
                                 global_work_offset, newGlobalSize,
                                 newLocalSize, num_events_in_wait_list,
                                 event_wait_list, event);
}
