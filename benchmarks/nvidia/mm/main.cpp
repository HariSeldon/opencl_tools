#include <iostream>
#include <sstream>
#include <string>
#include <iostream>

#include "Buffer.h"
#include "Device.h"
#include "Event.h"
#include "Kernel.h"
#include "Platform.h"
#include "Program.h"
#include "Queue.h"
#include "SystemConfiguration.h"

#include "bench_support.h"

//-----------------------------------------------------------------------------
#define REPETITIONS 1
#define ELEMENT_LIMIT 5
#define DIMENSIONS 2
#define SIZE 16 * 32
#define MM "mm"
#define MM_LOCAL "mmLocal"

//-----------------------------------------------------------------------------
unsigned long computeExecutionTime(cl_event &event);
void initialization(int argc, char **argv);
void hostMemoryAlloc();
void deviceMemoryAlloc();
void setKernelArguments();
void setKernelArgumentsWithLocal();
void setKernelArgumentsClassic();
void readResult();
void enqueWriteCommands(Queue &queue);
void enqueReadCommands(Queue &queue);
float random(float rand_min, float rand_max);
void freeMemory();
void printVector(const float *vector, unsigned int size);
void printMatrix(const float *matrix);
void setSizes();
void verifyResults();

//-----------------------------------------------------------------------------
// Runtime components.
Platform *platform;
Kernel *kernel;

// Host data.
float *hostA;
float *hostB;
float *hostC;

// Device data.
Buffer *A;
Buffer *B;
Buffer *C;

cl_uint *height = NULL;
cl_uint *width = NULL;

size_t *localWorkSize = NULL;
size_t *globalWorkSize = NULL;

std::string kernelName = "";
unsigned int blockSizeX = 0;
unsigned int blockSizeY = 0;
unsigned int unrollFactor = 0;

int PLATFORM_ID = 0;
int DEVICE_ID = 0;

size_t *newLS;
size_t *newGS;

//-----------------------------------------------------------------------------
int main(int argc, char **argv) {
  initialization(argc, argv);
  platform = new Platform(PLATFORM_ID);
  Context *context = platform->getContext();
  Device device = platform->getDevice(DEVICE_ID);
  setSizes();
  hostMemoryAlloc();
  deviceMemoryAlloc();
  Program program(context, KERNEL_DIRECTORY "/mm.cl");
  Queue queue(*context, device, Queue::EnableProfiling);
  enqueWriteCommands(queue);

  std::cout << "Running on: " << device.getName() << "\n";

  if (!program.build(device)) {
    std::cout << "Error building the program\n";
    std::cout << program.getBuildLog(device) << "\n";
    return 1;
  }
  kernel = program.createKernel(kernelName.c_str());

  setKernelArguments();

  long executionTime = 0l;
  for (unsigned int repetition = 0; repetition < REPETITIONS; ++repetition) {
    if (localWorkSize[0] == -1 || localWorkSize[1] == -1)
      localWorkSize = NULL;

    float real_time, proc_time, mflops = 0.0f;
    long long flops = 0;

    queue.run(*kernel, DIMENSIONS, 0, globalWorkSize, localWorkSize);
    queue.finish();
  }

  enqueReadCommands(queue);
  verifyResults();
  freeMemory();
  return 0;
}

//-----------------------------------------------------------------------------
void initialization(int argc, char **argv) {
  if (argc < 2) {
    std::cout << "Expected kernel name\n";
    exit(1);
  }

  srand((unsigned)time(0));

  kernelName = std::string(argv[1]);
  blockSizeX = 16;
  blockSizeY = 16;
  getPlatformDevice(&PLATFORM_ID, &DEVICE_ID);
}

//-----------------------------------------------------------------------------
void setSizes() {
  localWorkSize = new size_t[2];
  globalWorkSize = new size_t[2];

  if (kernelName == MM || kernelName == MM_LOCAL) {
    localWorkSize[0] = blockSizeX;
    localWorkSize[1] = blockSizeX;
    globalWorkSize[0] = SIZE;
    globalWorkSize[1] = SIZE;

    size_t *newGS = (size_t *)malloc(2 * sizeof(size_t));
    size_t *newLS = (size_t *)malloc(2 * sizeof(size_t));

    getNewSizes(globalWorkSize, localWorkSize, newGS, newLS, kernelName.c_str(),
                2);

    localWorkSize[0] = newLS[0];
    localWorkSize[1] = newLS[1];
    globalWorkSize[0] = newGS[0];
    globalWorkSize[1] = newGS[1];
  }
}

//-----------------------------------------------------------------------------
void freeMemory() {
  delete[] hostA;
  delete[] hostB;
  delete[] hostC;
  delete A;
  delete B;
  delete C;
  delete kernel;
  delete platform;
  delete width;
  delete height;
  delete[] localWorkSize;
  delete[] globalWorkSize;
}

//-----------------------------------------------------------------------------
void hostMemoryAlloc() {
  hostA = new float[globalWorkSize[0] * globalWorkSize[1]];
  hostB = new float[globalWorkSize[0] * globalWorkSize[1]];
  hostC = new float[globalWorkSize[0] * globalWorkSize[1]];

  int counter = 1;
  for (unsigned int row = 0; row < globalWorkSize[1]; row++) {
    for (unsigned int column = 0; column < globalWorkSize[0]; column++) {
      hostA[column + globalWorkSize[0] * row] =
          random(-ELEMENT_LIMIT, ELEMENT_LIMIT);
      hostB[column + globalWorkSize[0] * row] =
          random(-ELEMENT_LIMIT, ELEMENT_LIMIT);
      counter++;
      if (row == column) {
        hostB[column + globalWorkSize[0] * row] = 1;
      }
    }
  }
}

//-----------------------------------------------------------------------------
void deviceMemoryAlloc() {
  C = new Buffer(*(platform->getContext()), Buffer::WriteOnly,
                 globalWorkSize[0] * globalWorkSize[1] * sizeof(float), NULL);
  A = new Buffer(*(platform->getContext()), Buffer::ReadOnly,
                 globalWorkSize[0] * globalWorkSize[1] * sizeof(float), NULL);
  B = new Buffer(*(platform->getContext()), Buffer::ReadOnly,
                 globalWorkSize[0] * globalWorkSize[1] * sizeof(float), NULL);
  width = new cl_uint(globalWorkSize[0]);
  height = new cl_uint(globalWorkSize[1]);
}

//-----------------------------------------------------------------------------
void enqueWriteCommands(Queue &queue) {
  queue.writeBuffer(*A, globalWorkSize[0] * globalWorkSize[1] * sizeof(float),
                    (void *)hostA);
  queue.writeBuffer(*B, globalWorkSize[0] * globalWorkSize[1] * sizeof(float),
                    (void *)hostB);
  queue.finish();
}

//-----------------------------------------------------------------------------
void enqueReadCommands(Queue &queue) {
  queue.readBuffer(*C, globalWorkSize[0] * globalWorkSize[1] * sizeof(float),
                   (void *)hostC);
  queue.finish();
}

//-----------------------------------------------------------------------------
void setKernelArguments() {
  if (kernelName == MM)
    setKernelArgumentsClassic();
  else if (kernelName == MM_LOCAL)
    setKernelArgumentsWithLocal();
}

//-----------------------------------------------------------------------------
void setKernelArgumentsWithLocal() {
  kernel->setArgument(0, *A);
  kernel->setArgument(1, *B);
  kernel->setArgument(2, *C);
  kernel->setArgument(3, localWorkSize[0] * localWorkSize[1] * sizeof(float),
                      NULL);
  kernel->setArgument(4, localWorkSize[0] * localWorkSize[1] * sizeof(float),
                      NULL);
  kernel->setArgument(5, sizeof(cl_int), (void *)width);
  kernel->setArgument(6, sizeof(cl_int), (void *)height);
}

//-----------------------------------------------------------------------------
void setKernelArgumentsClassic() {
  kernel->setArgument(0, *A);
  kernel->setArgument(1, *B);
  kernel->setArgument(2, *C);
  kernel->setArgument(3, sizeof(cl_int), (void *)width);
  kernel->setArgument(4, sizeof(cl_int), (void *)height);
}

//-----------------------------------------------------------------------------
void verifyResults() {
  float *cpuHostC = new float[globalWorkSize[0] * globalWorkSize[1]];
  for (unsigned int row = 0; row < globalWorkSize[1]; ++row) {
    for (unsigned int column = 0; column < globalWorkSize[0]; ++column) {
      cpuHostC[row * globalWorkSize[0] + column] = 0.0f;
      for (unsigned int index = 0; index < globalWorkSize[0]; ++index)
        cpuHostC[row * globalWorkSize[0] + column] +=
            hostA[row * globalWorkSize[0] + index] *
            hostB[index * globalWorkSize[0] + column];

      if (abs(hostC[row * globalWorkSize[0] + column] -
              cpuHostC[row * globalWorkSize[0] + column]) >=
          0.001f) {
        std::cout << "Error in the computation\n";
        exit(1);
      }
    }
  }

  delete[] cpuHostC;
}

//-----------------------------------------------------------------------------
float random(float rand_min, float rand_max) {
  float result = (float)rand() / (float)RAND_MAX;
  return ((1.0 - result) * rand_min + result * rand_max);
}

//-----------------------------------------------------------------------------
void printVector(const float *printVector, unsigned int size) {
  for (unsigned int index = 0; index < size; ++index) {
    std::cout << printVector[index] << " ";
  }
  std::cout << std::endl;
}

//-----------------------------------------------------------------------------
void printMatrix(const float *matrix) {
  for (unsigned int row = 0; row < SIZE; ++row) {
    for (unsigned int column = 0; column < SIZE; ++column) {
      std::cout << matrix[row * SIZE + column] << " ";
    }
    std::cout << std::endl;
  }
}
