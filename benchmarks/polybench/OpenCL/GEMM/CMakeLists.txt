get_filename_component(CURRENT_BENCH ${CMAKE_CURRENT_SOURCE_DIR} NAME) 

set(EXE_NAME ${CURRENT_BENCH})

file(GLOB SRC_FILES_LIST "*.cpp")
file(GLOB COMMON_FILES_LIST ${COMMON_SRC_PATH}/*.cpp)

configure_file("${COMMON_INCLUDE_PATH}/SystemConfig.h.cmake"
               "${CMAKE_BINARY_DIR}/${BENCHMARKS_DIR}/${POLYBECH_DIR}/common/include/SystemConfig.h")


include_directories("${CMAKE_BINARY_DIR}/${BENCHMARKS_DIR}/${POLYBECH_DIR}/common/include/")
include_directories(${COMMON_INCLUDE_PATH})

add_executable(${EXE_NAME} ${SRC_FILES_LIST} ${COMMON_FILES_LIST})
set_target_properties(${EXE_NAME} PROPERTIES COMPILE_FLAGS -DKERNEL_PATH="\\"${CMAKE_CURRENT_SOURCE_DIR}\\"")

#-------------------------------------------------------------------------------
# Libraries.

# OpenCL library.
target_link_libraries(${EXE_NAME} ${OPENCL_LIB})

# BenchSupport library.
link_directories(${CMAKE_BINARY_DIR}/${TOOLS_DIR}/${BENCH_SUPPORT_LIB_DIR})
target_link_libraries(${EXE_NAME} ${BENCH_SUPPORT_LIB})
