opencl_tools
============

Super-project to collect opencl-related tools.

Requirements
------------

opencl_tools requires OpenCL. 

Installation
------------

To build these tools, 

```
#!plain

$> mkdir build
$> cd build
$> cmake .. -DOPENCL_INCLUDE_PATH=/path/to/opencl/include/ -DOPENCL_LIB_DIR_PATH=/path/to/opencl/library
$> make
```


Content
-------

* **benchmarks**
  * nvidia
    * mm (matrix multiplication)
    * mt (matrix transposition)
    * mv (matrix vector multiplication)
  
  * polybench
   

* **tools**
  * bench_support 
    Set of utility functions that can be used by benchmarks to change the global and local work size.
    The benchmarks in the nvidia and polybench directory make use of these functions.
    Example of usage:
    * export mm_GS=512,512 (set the global work size for the kernel named mm, if the kernel is 1D use only one number). 
    * export mm_LS=8,8 (set the local work size for the kernel named mm, if the kernel is 1D use only one number).
    * ./mm mm (run the program)
  * device_query
    Utility program to query the available OpenCL devices. 
    If this program shows stuff it means that OpenCL is working properly.
  * function_overload
    Utility library to take measurements of kernel execution time.
    Example of usage:
    * export LD_PRELOAD=/path/to/tools/function_overload/liboclwrapper.so
    * ./run_opencl_program
    * You should see in stdout information about the NDRange space and in stderr the kernel name and its exeuction time.
    * To repeat muliple times the kernel invoation use: export OCL_REPETITIONS=10 before the invocation.
  * opencl_wrapper 
    C++ wrapper written by [Alberto Magni][1].
    The nvidia benchmarks mm, mt and mv are written using this wrapper.

Author
-----

For any questions write me: [Alberto Magni][1]

[1]: mailto:alberto.magni86@gmail.com